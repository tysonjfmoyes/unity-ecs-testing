﻿using Unity.Entities;

namespace DOTSTesting.Rotating
{
    public struct Rotate : IComponentData
    {
        public float radiansPerSecond;
    }
}

