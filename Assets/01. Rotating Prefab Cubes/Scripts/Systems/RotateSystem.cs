﻿using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

namespace DOTSTesting.Rotating
{
    public class RotateSystem : JobComponentSystem
    {
        //[BurstCompile]
        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            var job = new RotateJob { deltaTime = Time.DeltaTime };
            return job.Schedule(this, inputDeps);
        }

        private struct RotateJob : IJobForEach<RotationEulerXYZ, Rotate> {
            public float deltaTime;
            public void Execute(ref RotationEulerXYZ euler, ref Rotate rotate) =>
                euler.Value.y += rotate.radiansPerSecond * deltaTime;
        }
    }
}
