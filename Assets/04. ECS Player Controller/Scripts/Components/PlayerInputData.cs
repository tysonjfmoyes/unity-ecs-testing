﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct PlayerInputData : IComponentData
{
    public float inputX, inputY;

    //public bool jumpPressed;
}
