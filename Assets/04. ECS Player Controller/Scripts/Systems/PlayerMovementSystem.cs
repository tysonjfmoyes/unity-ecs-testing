﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Physics;

[AlwaysSynchronizeSystem]
public class PlayerMovementSystem : ComponentSystem
{
    // Create local instances of necessary values.
    private float moveSpeed, jumpForce, maxSpeed;
    private float3 _target;

    protected override void OnUpdate() {
        Entities.ForEach((
            ref PlayerMovementData move,
            ref PlayerInputData input,
            ref Translation translation,
            ref PhysicsMass mass,
            ref PhysicsVelocity vel) =>
        {
                    _target.x = translation.Value.x + input.inputX;
                    _target.y = 0f;
                    _target.z = translation.Value.z + input.inputY;

                    mass.InverseInertia[0] = 0f;
                    mass.InverseInertia[2] = 0f;

                    var v = math.normalize(_target - translation.Value).xz * move.moveSpeed;

                    var l = vel.Linear;
                    l.x = v.x;
                    l.z = v.y;

                    if (math.SQRT2 * vel.Linear.x * vel.Linear.z < move.maxSpeed)
                        vel.Linear = l;

                    //if(input.jumpPressed) {
                    //   var impulse = new float3(0f, move.jumpForce, 0f);
                    //   vel.ApplyLinearImpulse(mass, impulse);
                    //}
        });
    }
}
