﻿using Unity.Entities;
using UnityEngine;

[AlwaysSynchronizeSystem]
public class PlayerInputSystem : ComponentSystem
{
    protected override void OnUpdate() {
        // Get the inputs
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");
        var jumpButton = Input.GetButtonDown("Jump");

        Entities.ForEach((ref PlayerInputData input) => {
            input.inputX = horizontal;
            input.inputY = vertical;
            //input.jumpPressed = jumpButton;
        });
    }
}
