﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using Unity.Mathematics;
using Time = UnityEngine.Time;

namespace DOTSTesting.Spawning
{
    public class SpawnerSystem : JobComponentSystem
    {
        private EndInitializationEntityCommandBufferSystem endSimulation;

        protected override void OnCreate() =>
            endSimulation = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();

        private struct SpawnerJob : IJobForEachWithEntity<Spawner, LocalToWorld>
        {
            private EntityCommandBuffer.Concurrent entityCommandBuffer;
            private readonly float deltaTime;
            private Random random;
            public SpawnerJob(EntityCommandBuffer.Concurrent entityCommandBuffer, Random random, float deltaTime) {
                this.deltaTime = deltaTime;
                this.random = random;
                this.entityCommandBuffer = entityCommandBuffer;
            }
            public void Execute(Entity entity, int index, ref Spawner spawner, [ReadOnly]ref LocalToWorld localToWorld) {
                spawner.secondsToNextSpawn -= deltaTime;

                if (spawner.secondsToNextSpawn >= 0) { return; }

                spawner.secondsToNextSpawn += spawner.secondsBetweenSpawns;

                Entity instance = entityCommandBuffer.Instantiate(index, spawner.prefab);
                entityCommandBuffer.SetComponent(index, instance, new Translation {
                    Value = localToWorld.Position + random.NextFloat3Direction() * random.NextFloat() * spawner.maxDistanceFromSpawner
                });
            }
        }
        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            var spawnerJob = new SpawnerJob(
                endSimulation.CreateCommandBuffer().ToConcurrent(),
                new Random((uint)UnityEngine.Random.Range(0, int.MaxValue)),
                Time.DeltaTime
            );

            JobHandle jobHandle = spawnerJob.Schedule(this, inputDeps);

            endSimulation.AddJobHandleForProducer(jobHandle);

            return jobHandle;
        }
    }
}
