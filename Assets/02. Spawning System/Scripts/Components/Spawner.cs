﻿using Unity.Entities;

namespace DOTSTesting.Spawning
{
    [GenerateAuthoringComponent]
    public struct Spawner : IComponentData
    {
        public Entity prefab;
        public float maxDistanceFromSpawner;
        public float secondsBetweenSpawns;
        public float secondsToNextSpawn;
    }
}
